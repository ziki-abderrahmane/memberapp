import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import deepLinking from './deepLinking';
import TabBarNavigation from '@navigations/tab-bar-navigation';

const Stack = createStackNavigator();

const Route = ({navigation}) => {
  return (
    <NavigationContainer linking={deepLinking}>
      <Stack.Navigator initialRouteName="Navigation">
        <Stack.Screen
          name="Navigation"
          component={TabBarNavigation}
          options={{
            headerShown: false,
          }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default Route;
