import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {
  Button,
  Text,
  TouchableNativeFeedback,
  TouchableWithoutFeedback,
  View,
} from 'react-native';
import HomeIcon from '@assets/icons/home.svg';
import WalletIcon from '@assets/icons/wallet.svg';
import ProfilIcon from '@assets/icons/profil.svg';
import PackageIcon from '@assets/icons/package.svg';
import NotifIcon from '@assets/icons/notif.svg';
import HomeScreen from '@screens/home';
import ProfileScreen from '@screens/profile';
import WorkOutScreen from '@screens/workout';
import Icon from 'react-native-vector-icons/MaterialIcons';
const HeaderComponent = ({navigation}) => {
  return (
    <TouchableWithoutFeedback onPress={() => navigation.goBack()}>
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          paddingHorizontal: 15,
        }}>
        <Icon name="arrow-back-ios" color="white" size={20} />
        <Text style={{color: 'white', marginLeft: 5}}>Back </Text>
      </View>
    </TouchableWithoutFeedback>
  );
};
const TabBarNavigation = ({navigation}) => {
  const Tab = createBottomTabNavigator();

  return (
    <Tab.Navigator
      initialRouteName="Home"
      screenOptions={({route}) => ({
        tabBarIcon: ({focused, color, size}) => {
          let iconColor;
          if (route.name == 'Home') {
            iconColor = focused ? '#FE7762' : 'white';
            return <HomeIcon stroke={iconColor} />;
          } else if (route.name == 'Profil') {
            iconColor = focused ? '#FE7762' : 'white';
            return <ProfilIcon stroke={iconColor} />;
          } else if (route.name == 'Notifications') {
            iconColor = focused ? '#FE7762' : 'white';
            return <NotifIcon stroke={iconColor} />;
          } else if (route.name == 'Wallet') {
            iconColor = focused ? '#FE7762' : 'white';
            return <WalletIcon stroke={iconColor} />;
          } else {
            iconColor = focused ? '#FE7762' : 'white';
            return <PackageIcon stroke={iconColor} />;
          }
        },
        tabBarLabelStyle: {
          fontSize: 14,
        },
        tabBarStyle: {
          paddingVertical: 4,
          backgroundColor: 'black',
        },
        tabBarShowLabel: false,
      })}>
      <Tab.Screen
        name="Home"
        component={HomeScreen}
        options={{
          headerShown: true,
          tabBarShowLabel: false,
          headerTitle: '',
          headerStyle: {
            backgroundColor: 'black',
            height: 35,
          },
          headerLeft: () => <HeaderComponent navigation={navigation} />,
        }}
      />
      <Tab.Screen
        name="Wallet"
        component={HomeScreen}
        options={{
          headerTitle: '',
          headerShown: true,
          headerStyle: {
            backgroundColor: 'black',
            height: 35,
          },
          headerLeft: () => <HeaderComponent navigation={navigation} />,
        }}
      />
      <Tab.Screen
        name="Profil"
        component={ProfileScreen}
        options={{
          headerShown: true,
          headerTitle: '',
          headerStyle: {
            backgroundColor: 'black',
            height: 35,
          },
          headerLeft: () => <HeaderComponent navigation={navigation} />,
        }}
      />
      <Tab.Screen
        name="Package"
        component={WorkOutScreen}
        options={{
          headerShown: true,
          headerStyle: {
            backgroundColor: 'black',
            height: 35,
          },
          headerTitle: '',
          headerLeft: () => <HeaderComponent navigation={navigation} />,
        }}
      />
      <Tab.Screen
        name="Notifications"
        component={HomeScreen}
        options={{
          headerShown: true,
          headerTitle: '',
          headerStyle: {
            backgroundColor: 'black',
            height: 35,
          },
          headerLeft: () => <HeaderComponent navigation={navigation} />,
        }}
      />
    </Tab.Navigator>
  );
};

export default TabBarNavigation;
