import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableWithoutFeedback,
  Image,
} from 'react-native';
import DoneIcon from '@assets/icons/done.svg';
import PlayIcon from '@assets/icons/play.svg';
import ScannButtonComponent from '@components/scann-button';

const WorkOutScreen = () => {
  return (
    <View style={styles.container}>
      <View
        style={{
          borderBottomWidth: 1,
          borderBottomColor: '#252625',
          paddingBottom: 10,
        }}>
        <Text style={[styles.textStyle, {fontSize: 25}]}>My workout week</Text>
      </View>
      <View style={{marginTop: 10}}>
        <View style={styles.item}>
          <View>
            <Text style={[{color: 'white'}, styles.textStyle]}>
              20x Jump rope
            </Text>
            <Text style={styles.time}>02:00</Text>
          </View>
          <View style={{marginLeft: 'auto'}}>
            <DoneIcon />
          </View>
        </View>
        <View style={styles.item}>
          <View>
            <Text style={styles.textStyle}>20x Jump rope</Text>
            <Text style={styles.time}>02:00</Text>
          </View>
          <View style={{marginLeft: 'auto'}}>
            <DoneIcon />
          </View>
        </View>
        <View style={styles.item}>
          <View>
            <Text style={styles.textStyle}>
              20x Squat thrust split jumps rope
            </Text>
            <Text style={styles.time}>02:00</Text>
          </View>
          <View style={{marginLeft: 'auto'}}>
            <PlayIcon />
          </View>
        </View>
        <View style={styles.item}>
          <View>
            <Text style={styles.textStyle}>
              20x Squat thrust split jumps rope
            </Text>
            <Text style={styles.time}>02:00</Text>
          </View>
          <View style={{marginLeft: 'auto'}}>
            <PlayIcon />
          </View>
        </View>
      </View>
      <View style={{position: 'absolute', bottom: 25, right: 20}}>
        <ScannButtonComponent />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 20,
    paddingVertical: 10,
    backgroundColor: 'black',
    flex: 1,
  },
  item: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 25,
    paddingVertical: 20,
    backgroundColor: '#252625',
    marginVertical: 10,
    borderRadius: 10,
  },
  time: {
    color: '#9F9F9F',
    fontWeight: '500',
    fontFamily: 'Poppins-Regular',
  },
  textStyle: {
    fontFamily: 'Poppins-Regular',
    color: 'white',
  },
});

export default WorkOutScreen;
