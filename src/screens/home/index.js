import React from 'react';
import {
  View,
  Text,
  Image,
  Dimensions,
  TouchableNativeFeedback,
  StyleSheet,
  ScrollView,
} from 'react-native';
import ReloadIcon from '@assets/icons/reload.svg';
import PreviousIcon from '@assets/icons/previous.svg';
import NextIcon from '@assets/icons/next.svg';
import PauseIcon from '@assets/icons/pause.svg';
import StopIcon from '@assets/icons/stop.svg';
import ScannButtonComponent from '@components/scann-button';
import LinearGradient from 'react-native-linear-gradient';

const HomeScreen = () => {
  return (
    <View style={{flex: 1}}>
      <View>
        <Image
          style={{
            width: Dimensions.get('screen').width,
            height: Dimensions.get('screen').height / 2,
          }}
          source={require('@assets/images/image.jpg')}
        />
      </View>
      <ScrollView style={styles.content}>
        <View style={styles.informations}>
          <View style={{paddingVertical: 10, paddingHorizontal: 15}}>
            <Text style={{color: '#fff', fontFamily: 'Poppins-Regular'}}>
              20x Squat thrust split jumps
            </Text>
            <Text style={{color: '#fff', fontFamily: 'Poppins-Regular'}}>
              Details about it
            </Text>
          </View>
          <LinearGradient
            colors={['#060606a1', '#252625e0', '#252625']}
            start={{x: 0, y: 1}}
            end={{x: 1, y: 1}}
            style={styles.gradientContent}>
            <Text style={styles.percentage}>65%</Text>
          </LinearGradient>
        </View>
        <View>
          <View style={{paddingVertical: 15}}>
            <Text
              style={{
                textAlign: 'center',
                color: 'white',
                fontWeight: '500',
                fontSize: 17,
                fontFamily: 'Poppins-Regular',
              }}>
              01:25
            </Text>
          </View>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-evenly',
            }}>
            <View>
              <TouchableNativeFeedback>
                <ReloadIcon />
              </TouchableNativeFeedback>
            </View>
            <View>
              <TouchableNativeFeedback>
                <PreviousIcon />
              </TouchableNativeFeedback>
            </View>
            <View style={styles.pauseContainer}>
              <TouchableNativeFeedback>
                <View style={styles.pauseBtn}>
                  <PauseIcon />
                </View>
              </TouchableNativeFeedback>
            </View>
            <View>
              <TouchableNativeFeedback>
                <NextIcon />
              </TouchableNativeFeedback>
            </View>
            <View>
              <TouchableNativeFeedback>
                <StopIcon />
              </TouchableNativeFeedback>
            </View>
          </View>
        </View>
      </ScrollView>
      <View style={{position: 'absolute', bottom: 25, right: 20}}>
        <ScannButtonComponent />
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  content: {
    marginTop: -30,
    backgroundColor: 'black',
    flex: 1,
    paddingHorizontal: 25,
    paddingTop: 15,
  },
  informations: {
    backgroundColor: '#FE7762',
    borderRadius: 10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  gradientContent: {
    height: '100%',
    borderTopRightRadius: 9,
    borderBottomRightRadius: 9,
    width: 80,
  },
  pauseContainer: {
    backgroundColor: '#47434c',
    borderRadius: 100,
    height: 50,
    width: 50,
    justifyContent: 'center',
    alignItems: 'center',
  },
  pauseBtn: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#7368FF',
    borderRadius: 100,
    height: 40,
    width: 40,
  },
  percentage: {
    color: '#FE7762',
    fontSize: 24,
    textAlign: 'center',
    marginTop: 14,
    fontFamily: 'Poppins-Regular',
  },
});
export default HomeScreen;
