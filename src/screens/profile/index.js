import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableWithoutFeedback,
  Image,
  ScrollView,
} from 'react-native';
import ClockIcon from '@assets/icons/clock.svg';
import DiamondIcon from '@assets/icons/diamond.svg';
import CaloriesIcon from '@assets/icons/calories.svg';
import CameraIcon from '@assets/icons/camera.svg';

const ProfileScreen = () => {
  return (
    <View style={styles.container}>
      <Text style={[styles.textInformation, {fontSize: 30}]}>My profil</Text>
      <View style={styles.flexContent}>
        <View style={{marginVertical: 15, position: 'relative'}}>
          <Image
            style={{borderRadius: 100, height: 120, width: 120}}
            source={require('@assets/images/user.jpg')}
          />
          <View style={{position: 'absolute', bottom: 10, right: -3}}>
            <TouchableWithoutFeedback>
              <View style={styles.btnUpdateImage}>
                <CameraIcon />
              </View>
            </TouchableWithoutFeedback>
          </View>
        </View>
        <Text
          style={{
            color: 'white',
            textTransform: 'capitalize',
            fontFamily: 'Poppins-Regular',
          }}>
          jeniffer winget
        </Text>
        <Text
          style={{
            color: '#9F9F9F',
            marginVertical: 4,
            fontFamily: 'Poppins-Regular',
          }}>
          Beginner
        </Text>
        <TouchableWithoutFeedback>
          <View style={styles.btnEditInfo}>
            <Text style={styles.textInformation}>Edit profil</Text>
          </View>
        </TouchableWithoutFeedback>
      </View>

      <ScrollView showsVerticalScrollIndicator={false} style={{marginTop: 15}}>
        <View style={styles.item}>
          <View>
            <Text style={styles.itemTitle}>Total time</Text>
            <Text style={styles.textInformation}>02:30:15</Text>
          </View>
          <View style={{marginLeft: 'auto'}}>
            <ClockIcon />
          </View>
        </View>
        <View style={styles.item}>
          <View>
            <Text style={styles.itemTitle}>Burned calories</Text>
            <Text style={styles.textInformation}>7200 cal</Text>
          </View>
          <View style={{marginLeft: 'auto'}}>
            <CaloriesIcon />
          </View>
        </View>
        <View style={styles.item}>
          <View>
            <Text style={styles.itemTitle}>Points collected</Text>
            <Text style={styles.textInformation}>1200 points</Text>
          </View>
          <View style={{marginLeft: 'auto'}}>
            <DiamondIcon />
          </View>
        </View>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 25,
    paddingVertical: 5,
    backgroundColor: 'black',
    flex: 1,
  },
  flexContent: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
  },
  item: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 25,
    paddingVertical: 20,
    backgroundColor: '#252625',
    marginVertical: 10,
    borderRadius: 10,
  },
  itemTitle: {
    color: '#9F9F9F',
    fontFamily: 'Poppins-Regular',
  },
  textInformation: {
    color: 'white',
    fontWeight: '500',
    fontFamily: 'Poppins-Regular',
  },
  btnUpdateImage: {
    backgroundColor: '#252625',
    borderRadius: 100,
    height: 30,
    width: 30,
    alignItems: 'center',
    justifyContent: 'center',
  },
  btnEditInfo: {
    backgroundColor: '#FE7762',
    paddingHorizontal: 12,
    paddingVertical: 7,
    borderRadius: 20,
  },
});

export default ProfileScreen;
