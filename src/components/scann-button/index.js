import React from 'react';
import {View, TouchableNativeFeedback} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

const ScannButtonComponent = () => {
  return (
    <TouchableNativeFeedback>
      <View
        style={{
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: '#7368FF',
          borderRadius: 100,
          height: 50,
          width: 50,
        }}>
        <Icon name="qrcode" color="white" size={30} />
      </View>
    </TouchableNativeFeedback>
  );
};
export default ScannButtonComponent;
